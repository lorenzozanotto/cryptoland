//
//  TransfersScene.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 03/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import SwiftUI
import PromiseKit
import web3

struct TransfersScene: DynamicScene {
    typealias ViewModel = TransfersViewModel
    typealias Value = [EthereumTransfer]

    @EnvironmentObject var viewModel: ViewModel

    func getDataView(for value: Value) -> AnyView {
        return AnyView(
            NavigationView {
                List {
                    ForEach(value, content: { transfer in
                        TransferCell(transfer: transfer)
                    })
                }
                .listStyle(GroupedListStyle())
            }
        )
    }

    var body: some View {
        content.onAppear {
            self.viewModel.getTransfers()
        }
    }
}

struct TransferCell: View {
    var transfer: EthereumTransfer

    var body: some View {
        VStack {
            HStack {
                Text("🚀 From").bold()
                Spacer()
                Text(transfer.from.truncated)
            }
            Spacer()
            HStack {
                Text("🏋🏻‍♂️ Hash").bold()
                Spacer()
                Text(transfer.id.truncated)
            }
            Spacer()
            HStack {
                Text("📦 Block").bold()
                Spacer()
                Text(transfer.block)
            }
            Spacer()
            HStack {
                Text("💰 Value").bold()
                Spacer()
                Text(transfer.amount.description)
            }
        }
    }
}

struct TransfersScene_Previews: PreviewProvider {
    static var previews: some View {
        TransfersScene()
    }
}
