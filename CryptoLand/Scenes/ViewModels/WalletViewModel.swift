//
//  WalletViewModel.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import PromiseKit

final class WalletViewModel: ObservableDataFetchingViewModel {
    @Published var state: DataFetchingViewModelState<EthereumBalance> = .loading

    @Published var isAlertPresented = false
    @Published var latestTxHash = ""

    var service: EthereumService

    init(_ service: EthereumService) {
        self.service = service
    }

    func fetchBalance() {
        state = .loading
        service.balance()
            .done { balance in
                self.state = .data(balance)
            }.catch { error in
                self.state = .error(error)
            }
    }

    func sendTransaction(to address: String) {
        firstly {
            service.send(to: address)
        }.done { txHash in
            self.isAlertPresented.toggle()
            self.latestTxHash = txHash
            UIPasteboard.general.string = txHash
        }.cauterize()
    }
}
