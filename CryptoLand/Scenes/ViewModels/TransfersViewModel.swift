//
//  TransfersViewModel.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 03/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import BigInt

final class TransfersViewModel: ObservableDataFetchingViewModel {
    @Published var state: DataFetchingViewModelState<[EthereumTransfer]> = .loading

    var service: EthereumService

    init(_ service: EthereumService) {
        self.service = service
    }

    func getTransfers() {
        service.transfers()
            .done { transfers in
                let ethTransfers = transfers.map {
                    EthereumTransfer(
                        id: $0.log.transactionHash ?? "",
                        from: $0.from.value,
                        to: $0.to.value,
                        amount: BigInt($0.value),
                        block: $0.log.blockNumber.stringValue
                    )
                }
                self.state = .data(ethTransfers)
            }
            .catch { error in
                self.state = .error(error)
            }
    }
}
