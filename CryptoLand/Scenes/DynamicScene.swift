//
//  DynamicScene.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

enum DataFetchingViewModelState<T> {
    case loading
    case error(Error)
    case noData
    case data(T)
}

protocol DataFetchingViewModel {
    associatedtype Value
    var state: DataFetchingViewModelState<Value> { get }
}

internal typealias ObservableDataFetchingViewModel = ObservableObject & DataFetchingViewModel

protocol DynamicScene: View {
    associatedtype ViewModel: ObservableDataFetchingViewModel
    var viewModel: ViewModel { get }
    func getDataView(for value: ViewModel.Value) -> AnyView
}

extension DynamicScene {
    var content: some View {
        switch viewModel.state {
        case .loading:
            return AnyView(Text("Loading"))
        case .error:
            return AnyView(Text("Error"))
        case .noData:
            return AnyView(Text("Empty"))
        case .data(let value):
            return getDataView(for: value)
        }
    }
}
