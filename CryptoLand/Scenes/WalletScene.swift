//
//  ContentView.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import SwiftUI

struct WalletScene: DynamicScene {
    typealias ViewModel = WalletViewModel
    typealias Value = EthereumBalance

    @EnvironmentObject var viewModel: ViewModel

    @State private var showModal: Bool = false
    @State private var address: String = ""

    var transfersScreen: some View {
        TransfersScene().environmentObject(TransfersViewModel(viewModel.service))
    }

    func getDataView(for value: Value) -> AnyView {
        return AnyView(
            NavigationView {
                VStack {
                    Spacer()
                    Text("Wallet Balance")
                        .font(.headline)
                    Text("\(value.literal) ETH")
                        .font(.title)

                    TextField("Enter your Ropsten address!", text: $address)
                        .textFieldStyle(RoundedBorderTextFieldStyle())

                    Button(action: {
                        self.viewModel.sendTransaction(to: self.address)
                    }) {
                        Text("Send 0.01 ETH")
                    }
                    .alert(isPresented: $viewModel.isAlertPresented) {
                        Alert(
                            title: Text("Your hash has been copied to the clipboard ❤️"),
                            message: Text(viewModel.latestTxHash)
                        )
                    }
                    Spacer()
                    Button(action: {
                        self.showModal = true
                    }) {
                        Text("View ERC20 Transfers")
                    }.sheet(isPresented: self.$showModal) {
                        self.transfersScreen
                    }
                    Spacer()
                }.padding(24)
            }
        )
    }

    var body: some View {
        content.onAppear {
            self.viewModel.fetchBalance()
        }
    }
}

struct WalletScene_Previews: PreviewProvider {
    static var previews: some View {
        WalletScene()
    }
}
