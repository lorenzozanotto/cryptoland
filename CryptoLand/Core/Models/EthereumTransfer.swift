//
//  EthereumTransfer.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 03/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import BigInt

struct EthereumTransfer: Identifiable {
    var id: String
    let from: String
    let to: String
    let amount: BigInt
    let block: String
}
