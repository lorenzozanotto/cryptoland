//
//  EthereumBalance.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import BigInt

struct EthereumBalance {
    private let balance: BigInt

    var literal: String {
        EthereumNumberFormatter().string(from: balance)
    }

    init(balance: BigInt) {
        self.balance = balance
    }
}
