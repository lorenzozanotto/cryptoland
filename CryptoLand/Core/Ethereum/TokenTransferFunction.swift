//
//  TokenTransferFunction.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3
import BigInt

struct TokenTransferFunction: ABIFunction {
    static var name: String = "transferToken"

    var gasPrice: BigUInt?
    var gasLimit: BigUInt? = BigUInt(250000)
    var contract: EthereumAddress = EthereumAddress(Constants.transferTokenAddress)
    var from: EthereumAddress?

    let wallet: EthereumAddress
    let token: EthereumAddress
    let to: EthereumAddress
    let amount: BigUInt
    let data: Data

    init(gasPrice: BigUInt, wallet: EthereumAddress, token: EthereumAddress, to: EthereumAddress, amount: BigUInt) {
        self.gasPrice = gasPrice
        self.wallet = wallet
        self.token = token
        self.to = to
        self.amount = amount
        self.data = Data()
    }

    func encode(to encoder: ABIFunctionEncoder) throws {
        try encoder.encode(wallet)
        try encoder.encode(token)
        try encoder.encode(to)
        try encoder.encode(amount)
        try encoder.encode(data)
    }
}
