//
//  PrivateKeyStorage.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3

final class PrivateKeyStorage: EthereumKeyStorageProtocol {
    let privateKey: String

    init(privateKey: String) {
        self.privateKey = privateKey
    }

    func storePrivateKey(key: Data) throws {

    }

    func loadPrivateKey() throws -> Data {
        privateKey.web3.hexData ?? Data()
    }
}
