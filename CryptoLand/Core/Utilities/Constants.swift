//
//  Constants.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation

struct Constants {

    /// Client
    static let infuraUrl = "https://ropsten.infura.io/v3/735489d9f846491faae7a31e1862d24b"

    /// Wallet
    static let ownerPrivateKey = "0xec983791a21bea916170ee0aead71ab95c13280656e93ea4124c447bbd9a24a2"
    static let walletAddress = "0x70ABd7F0c9Bdc109b579180B272525880Fb7E0cB"

    /// Ethereum
    static let ethAddress = "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE"
    static let transferTokenAddress = "0xcdAd167a8A9EAd2DECEdA7c8cC908108b0Cc06D1"

}
