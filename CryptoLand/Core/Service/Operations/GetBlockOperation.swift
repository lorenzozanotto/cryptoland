//
//  GetBlockOperation.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3
import BigInt
import PromiseKit

final class GetBlockOperation: BaseOperation<Void, Int> {
    override func execute() -> Promise<Int> {
        Promise { resolver in
            client.eth_blockNumber {
                self.resolve(with: resolver, $0, $1)
            }
        }
    }
}
