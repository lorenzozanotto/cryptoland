//
//  GasPriceOperation.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3
import BigInt
import PromiseKit

final class GasPriceOperation: BaseOperation<Void, BigUInt> {
    override func execute() -> Promise<BigUInt> {
        Promise { resolver in
            client.eth_gasPrice {
                self.resolve(with: resolver, $0, $1)
            }
        }
    }
}
