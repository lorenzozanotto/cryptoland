//
//  BaseOperation.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import PromiseKit
import web3

class BaseOperation<Input, Output> {
    var client: EthereumClient
    var input: Input

    init(_ client: EthereumClient, input: Input) {
        self.client = client
        self.input = input
    }

    func execute() -> Promise<Output> {
        fatalError("BaseOperation should not execute directly")
    }

    func resolve<Output>(with resolver: Resolver<Output>, _ first: Any?, _ second: Output?) {
        if let first = first as? Error {
            resolver.reject(first)
        }

        if let second = second {
            resolver.fulfill(second)
        }
    }
}
