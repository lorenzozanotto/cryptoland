//
//  SendTransactionOperation.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3
import BigInt
import PromiseKit

struct Transaction {
    let to: String
    let gasPrice: BigUInt
}

final class SendTransactionOperation: BaseOperation<Transaction, String> {
    var transferFunction: TokenTransferFunction {
        TokenTransferFunction(
            gasPrice: input.gasPrice,
            wallet: EthereumAddress(Constants.walletAddress),
            token: EthereumAddress(Constants.ethAddress),
            to: EthereumAddress(input.to),
            amount: BigUInt(10000000000000000)
        )
    }

    override func execute() -> Promise<String> {
        Promise { resolver in
            let transaction = try transferFunction.transaction()
            let keyStorage = PrivateKeyStorage(privateKey: Constants.ownerPrivateKey)
            let account = try EthereumAccount(keyStorage: keyStorage)

            client.eth_sendRawTransaction(transaction, withAccount: account) {
                self.resolve(with: resolver, $0, $1)
            }
        }
    }
}
