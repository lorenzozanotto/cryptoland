//
//  GetBalanceOperation.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3
import BigInt
import PromiseKit

final class GetBalanceOperation: BaseOperation<Int, BigUInt> {
    override func execute() -> Promise<BigUInt> {
        Promise { resolver in
            client.eth_getBalance(
                address: Constants.walletAddress,
                block: EthereumBlock(rawValue: input)
            ) {
                self.resolve(with: resolver, $0, $1)
            }
        }
    }
}
