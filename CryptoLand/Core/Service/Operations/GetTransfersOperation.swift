//
//  GetTransfersOperation.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 03/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import web3
import BigInt
import PromiseKit

final class GetTransfersOperation: BaseOperation<Int, [ERC20Events.Transfer]> {
    override func execute() -> Promise<[ERC20Events.Transfer]> {
        Promise { resolver in
            ERC20(client: client).transferEventsTo(
                recipient: EthereumAddress(Constants.walletAddress),
                fromBlock: EthereumBlock(rawValue: 0),
                toBlock: EthereumBlock(rawValue: input)) {
                    self.resolve(with: resolver, $0, $1)
                }
        }
    }
}
