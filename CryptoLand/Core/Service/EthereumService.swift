//
//  EthereumService.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 02/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import PromiseKit
import BigInt
import web3

/// MARK: - Ethereum capabilities
struct EthereumService {
    var client: EthereumClient {
        EthereumClient(url: URL(string: Constants.infuraUrl)!)
    }

    private func getBlock() -> Promise<Int> {
        GetBlockOperation(client, input: Void()).execute()
    }

    private func getBalance(block: Int) -> Promise<BigUInt> {
        GetBalanceOperation(client, input: block).execute()
    }

    private func getGasPrice() -> Promise<BigUInt> {
        GasPriceOperation(client, input: Void()).execute()
    }

    private func sendTransaction(to address: String, gasPrice: BigUInt) -> Promise<String> {
        let transaction = Transaction(to: address, gasPrice: gasPrice)
        return SendTransactionOperation(client, input: transaction).execute()
    }

    private func getTransfers(until block: Int) -> Promise<[ERC20Events.Transfer]> {
        GetTransfersOperation(client, input: block).execute()
    }
}

/// MARK: - Publicly available actions
extension EthereumService {
    func balance() -> Promise<EthereumBalance> {
        firstly {
            getBlock()
        }.then {
            self.getBalance(block: $0)
        }.map {
            EthereumBalance(balance: BigInt($0))
        }
    }

    func send(to: String) -> Promise<String> {
        firstly {
            getGasPrice()
        }.then {
            self.sendTransaction(to: to, gasPrice: $0)
        }
    }

    func transfers() -> Promise<[ERC20Events.Transfer]> {
        firstly {
            getBlock()
        }.then {
            self.getTransfers(until: $0)
        }
    }
}
