//
//  String.swift
//  CryptoLand
//
//  Created by Lorenzo Zanotto on 03/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import Foundation

extension String {
    var truncated: String {
        "..." + String(suffix(20))
    }
}
