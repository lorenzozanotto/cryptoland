
# Crypto Land

A lovely **SwiftUI** iOS app that enables you to see an Ethereum wallet balance, interact with a transfer funds smart contract and view ERC20 transactions. 

![enter image description here](https://gitlab.com/lorenzozanotto/cryptoland/-/raw/master/Resources/demo.gif)

## 🚀 Run the Project
Even though the project contains some CocoaPods dependencies, they've been added to source control so you can just download and run the project. 

This app uses SwiftUI, so you'll need a simulator running at least iOS 13.

## 🛠 Run Tests

The project has some tests, especially for the **balance formatting** and the **send operation**. 

Simply run the tests via Xcode, no further configuration is required

## ✨ Code highlights

I wanted to keep the application architecture itself simple, so I could focus on interacting with the Ethereum blockchain through **Web3.swift**.

This is why the project is using SwiftUI, combined with a ViewModel approach that handles mutating view properties to redraw the UI when needed.

### 🍀 Ethereum Service 

Each screen has access to the Ethereum Service, which is passed through dependency injection to the constructor. This module is responsible of creating a series of actions that will eventually interact with the web3.swift library.

The whole service is built to be lightweight and modular through the use of chainable operations.

### ⛓ Chainable Operations

We know that performing operations on the blockchain requires a series of complex actions that needs to be completed in sequence in order to work. Think about sending a transaction: you first need to get the current gas price for the network and only then you'll be able to send the transaction.

To make this happen, I used a Promises approach to chain operations. Retrieving the network gas price and sending a transaction is as easy as 

```swift
func send(to: String) -> Promise<String> {
  firstly {
    getGasPrice()
  }.then {
    self.sendTransaction(to: to, gasPrice: $0)
  }
}
```

### 💪 Modular Capabilities

The goal was to use a testable and modular approach to interact with the web3.swift library.

Each capability like getting the latest block number, querying the balance or sending a transaction... is wrapped inside it's own operation (`GetBlockOperation`, `GetBalanceOperation`...)

Their syntax is concise and explicative and serves as a reference for what the app is capable of doing on the Ethereum network. 

```swift
private func getBlock() -> Promise<Int> {
  GetBlockOperation(client, input: Void()).execute()
} 

private func getBalance(block: Int) -> Promise<BigUInt> {
  GetBalanceOperation(client, input: block).execute()
}

private func getGasPrice() -> Promise<BigUInt> {
  GasPriceOperation(client, input: Void()).execute()
}
```

### 📢 Declarative view state

Using SwiftUI helps in obtaining a declarative way of building interfaces. But what about view states like loading, displaying errors and empty data? 

The framework itself is so new that isn't able to provide us with the right tools. That needed a little bit of abstraction using an enum to represent the various view states and letting the ViewModel decide in which state we're in.

In this way SwiftUI could rebuild the whole interface as soon as the state changes.

```swift
enum  DataFetchingViewModelState<T> {
  case loading
  case error(Error)
  case noData
  case data(T)
}

func  fetchBalance() {
  state = .loading
  service.balance()
    .done { balance in
      self.state = .data(balance)
    }.catch { error in
      self.state = .error(error)
    }
}
```