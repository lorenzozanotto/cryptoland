//
//  EthereumNumberFormatterTests.swift
//  CryptoLandTests
//
//  Created by Lorenzo Zanotto on 03/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import XCTest
import BigInt
@testable import CryptoLand

class EthereumNumberFormatterTests: XCTestCase {

    var formatter: EthereumNumberFormatter {
        EthereumNumberFormatter(locale: Locale(identifier: "en_US"))
    }

    func testEthereumFormatting() {
        let balance = BigInt("13784290129482617283")

        let formatted = formatter.string(from: balance)
        let expected = "13.78"

        XCTAssertEqual(formatted, expected)
    }

    func testLongNumberWithCustomDecimals() {
        let balance = BigInt("137842901294826172831234")

        let formatted = formatter.string(from: balance, decimals: 5)
        let expected = "1,378,429,012,948,261,728.31"

        XCTAssertEqual(formatted, expected)
    }
}
