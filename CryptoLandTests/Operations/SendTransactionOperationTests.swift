//
//  SendTransactionOperationTests.swift
//  CryptoLandTests
//
//  Created by Lorenzo Zanotto on 04/02/2020.
//  Copyright © 2020 Lorenzo Zanotto. All rights reserved.
//

import XCTest
import BigInt
import web3
@testable import CryptoLand

class SendTransactionOperationTests: XCTestCase {

    var fakeAddress = "0xAbcdef"

    var fakeOperation: SendTransactionOperation {
        let input = Transaction(to: fakeAddress, gasPrice: 250000)
        return SendTransactionOperation(EthereumService().client, input: input)
    }

    func testRawTransactionGeneration() {

        // Given a transaction
        let rawTransaction = try? fakeOperation.transferFunction.transaction()

        // Expect
        XCTAssertNotNil(rawTransaction)
    }

    func testRawTransactionData() {

        // Given a transaction
        let rawTransaction = try? fakeOperation.transferFunction.transaction()

        // Assuming
        let expectedAddress = Constants.transferTokenAddress.lowercased()

        // Expect
        XCTAssertEqual(rawTransaction?.to.value.lowercased(), expectedAddress)
    }

    func testTransferFunctionData() {

        // Given a TransferFunction
        let transferFunction = fakeOperation.transferFunction

        // Assuming
        let toAddress = fakeAddress.lowercased()
        let amount: BigUInt = 10000000000000000

        // Expect
        XCTAssertEqual(transferFunction.to.value.lowercased(), toAddress)
        XCTAssertEqual(transferFunction.amount, amount)
    }

}
